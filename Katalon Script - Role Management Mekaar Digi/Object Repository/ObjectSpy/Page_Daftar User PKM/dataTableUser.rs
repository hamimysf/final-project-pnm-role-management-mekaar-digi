<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dataTableUser</name>
   <tag></tag>
   <elementGuidId>4f68dab6-938b-42be-b784-d230a35dfd7c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='getUserList']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#getUserList</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#getUserList</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4724dd63-24b4-4461-baad-2d9b424ea9cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>panel-body</value>
      <webElementGuid>f8ee0c4f-182a-40cb-a078-8f69edf9ccbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>getUserList</value>
      <webElementGuid>dbb4b05f-7519-48a5-83fc-30f22794b700</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                        
                                            Show 102550100 entriesSearch:
                                                
                                                    NoNamaNIPNama LengkapJabatanRoleCabangVirtual AccountStatus Aksi
                                                
                                                
                                                1AO03-91010MKR231625.03.21Ade RismaAccount OfficerAccount Officer91010222269101000012Aktif                                 AksiEdit2AO11-91010MKR495622.07.23NurmalasariAccount OfficerAccount Officer91010222269101000007Aktif                                 AksiEdit3AO12-91010MKR343833.06.22Nurul Agnis PrihartianiAccount OfficerAccount Officer91010222269101000011Aktif                                 AksiEdit4AO13-91010Account OfficerAccount Officer91010222269101000014Nonaktif                                 AksiEdit5AO14-91010MKR396322.09.22N NuraeniAccount OfficerAccount Officer91010222269101000015Aktif                                 AksiEdit6AO15-91010MKR379081.08.22Ibnu Hajar Saepul MuhtarAccount OfficerAccount Officer91010222269101000016Aktif                                 AksiEdit7AO16-91010Account OfficerAccount Officer91010222269134500025Nonaktif                                 AksiEdit8AO17-91010Account OfficerAccount Officer91010222269134500026Nonaktif                                 AksiEdit9AO18-91010MKR342860.06.22Titi WinarniAccount OfficerAccount Officer91010222269134500027Aktif                                 AksiEdit10AO19-91010MKR379306.08.22Virda Rahman FirmansyahAccount OfficerAccount Officer91010222269101000017Aktif                                 AksiEdit11AO20-91010MKR415707.11.22Gita PurnamasariAccount OfficerAccount Officer91010222269101000018Aktif                                 AksiEdit12AO21-91010MKR438848.02.23Lilim Halimatu ShadiahAccount OfficerAccount Officer91010222269101000019Aktif                                 AksiEdit13AO22-91010MKR438129.02.23Hilda NuryadiAccount OfficerAccount Officer91010222269101000020Aktif                                 AksiEdit14AO23-91010MKR483565.06.23Elsa Nur FadilahAccount OfficerAccount Officer91010222269101000021Aktif                                 AksiEdit15AO4-91010Account OfficerAccount Officer91010222269101000003Nonaktif                                 AksiEdit16AO5-91010MKR207135.11.20PebriyaniAccount OfficerAccount Officer91010222269101000001Aktif                                 AksiEdit17AO6-91010Account OfficerAccount Officer91010222269101000006Nonaktif                                 AksiEdit18AO7-91010MKR269958.08.21Resty Agnia NurohmahAccount OfficerAccount Officer91010222269101000004Aktif                                 AksiEdit19Bukan Role AO / SAOMKR130338.09.19Devi SintiasariFinance Administration OfficerFinance Administration Officer91010Nomor VA Tidak TersediaAktif                                 AksiView20Bukan Role AO / SAOMKR196193.10.20Siti RiyatiFinance Administration OfficerFinance Administration Officer91010Nomor VA Tidak TersediaAktif                                 AksiView21KC-9101022283.01.20Lia AmaliaKepala Cabang MekaarKepala Cabang91010222269101000009Aktif                                 AksiEdit22SAO02-91010MKR202512.11.20Rosah Riska DamayantiSenior Account OfficerSenior Account Officer91010222269101000013Aktif                                 AksiEdit23SAO1-91010Senior Account OfficerSenior Account Officer91010222269101000010Nonaktif                                 AksiEdit24SAO2-91010MKR174652.08.20Fani Nur OktavianiSenior Account OfficerSenior Account Officer91010222269101000008Aktif                                 AksiEdit25SAO3-91010MKR105350.05.19Enjellita AdestaniaSenior Account OfficerSenior Account Officer91010222269101000022Aktif                                 AksiEdit
                                            Showing 1 to 25 of 25 entriesPrevious1Next
                                        
                                    
                                </value>
      <webElementGuid>447f092a-1fe8-4799-b941-7add0173fcdb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;getUserList&quot;)</value>
      <webElementGuid>0b5fd0a9-5e07-4fff-8ecd-4b7fc1bbb974</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='getUserList']</value>
      <webElementGuid>85bbe8f9-e03c-4e5b-9d57-8a8f30b6f22b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div/div/div[2]/div[3]/div[3]</value>
      <webElementGuid>9dc05dbe-33f1-4155-aabe-4c8d0e1fe1ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[3]</value>
      <webElementGuid>f36cc5a2-3d5d-4dbf-b6b9-b10da6778ab7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'getUserList' and (text() = '
                                    
                                        
                                            Show 102550100 entriesSearch:
                                                
                                                    NoNamaNIPNama LengkapJabatanRoleCabangVirtual AccountStatus Aksi
                                                
                                                
                                                1AO03-91010MKR231625.03.21Ade RismaAccount OfficerAccount Officer91010222269101000012Aktif                                 AksiEdit2AO11-91010MKR495622.07.23NurmalasariAccount OfficerAccount Officer91010222269101000007Aktif                                 AksiEdit3AO12-91010MKR343833.06.22Nurul Agnis PrihartianiAccount OfficerAccount Officer91010222269101000011Aktif                                 AksiEdit4AO13-91010Account OfficerAccount Officer91010222269101000014Nonaktif                                 AksiEdit5AO14-91010MKR396322.09.22N NuraeniAccount OfficerAccount Officer91010222269101000015Aktif                                 AksiEdit6AO15-91010MKR379081.08.22Ibnu Hajar Saepul MuhtarAccount OfficerAccount Officer91010222269101000016Aktif                                 AksiEdit7AO16-91010Account OfficerAccount Officer91010222269134500025Nonaktif                                 AksiEdit8AO17-91010Account OfficerAccount Officer91010222269134500026Nonaktif                                 AksiEdit9AO18-91010MKR342860.06.22Titi WinarniAccount OfficerAccount Officer91010222269134500027Aktif                                 AksiEdit10AO19-91010MKR379306.08.22Virda Rahman FirmansyahAccount OfficerAccount Officer91010222269101000017Aktif                                 AksiEdit11AO20-91010MKR415707.11.22Gita PurnamasariAccount OfficerAccount Officer91010222269101000018Aktif                                 AksiEdit12AO21-91010MKR438848.02.23Lilim Halimatu ShadiahAccount OfficerAccount Officer91010222269101000019Aktif                                 AksiEdit13AO22-91010MKR438129.02.23Hilda NuryadiAccount OfficerAccount Officer91010222269101000020Aktif                                 AksiEdit14AO23-91010MKR483565.06.23Elsa Nur FadilahAccount OfficerAccount Officer91010222269101000021Aktif                                 AksiEdit15AO4-91010Account OfficerAccount Officer91010222269101000003Nonaktif                                 AksiEdit16AO5-91010MKR207135.11.20PebriyaniAccount OfficerAccount Officer91010222269101000001Aktif                                 AksiEdit17AO6-91010Account OfficerAccount Officer91010222269101000006Nonaktif                                 AksiEdit18AO7-91010MKR269958.08.21Resty Agnia NurohmahAccount OfficerAccount Officer91010222269101000004Aktif                                 AksiEdit19Bukan Role AO / SAOMKR130338.09.19Devi SintiasariFinance Administration OfficerFinance Administration Officer91010Nomor VA Tidak TersediaAktif                                 AksiView20Bukan Role AO / SAOMKR196193.10.20Siti RiyatiFinance Administration OfficerFinance Administration Officer91010Nomor VA Tidak TersediaAktif                                 AksiView21KC-9101022283.01.20Lia AmaliaKepala Cabang MekaarKepala Cabang91010222269101000009Aktif                                 AksiEdit22SAO02-91010MKR202512.11.20Rosah Riska DamayantiSenior Account OfficerSenior Account Officer91010222269101000013Aktif                                 AksiEdit23SAO1-91010Senior Account OfficerSenior Account Officer91010222269101000010Nonaktif                                 AksiEdit24SAO2-91010MKR174652.08.20Fani Nur OktavianiSenior Account OfficerSenior Account Officer91010222269101000008Aktif                                 AksiEdit25SAO3-91010MKR105350.05.19Enjellita AdestaniaSenior Account OfficerSenior Account Officer91010222269101000022Aktif                                 AksiEdit
                                            Showing 1 to 25 of 25 entriesPrevious1Next
                                        
                                    
                                ' or . = '
                                    
                                        
                                            Show 102550100 entriesSearch:
                                                
                                                    NoNamaNIPNama LengkapJabatanRoleCabangVirtual AccountStatus Aksi
                                                
                                                
                                                1AO03-91010MKR231625.03.21Ade RismaAccount OfficerAccount Officer91010222269101000012Aktif                                 AksiEdit2AO11-91010MKR495622.07.23NurmalasariAccount OfficerAccount Officer91010222269101000007Aktif                                 AksiEdit3AO12-91010MKR343833.06.22Nurul Agnis PrihartianiAccount OfficerAccount Officer91010222269101000011Aktif                                 AksiEdit4AO13-91010Account OfficerAccount Officer91010222269101000014Nonaktif                                 AksiEdit5AO14-91010MKR396322.09.22N NuraeniAccount OfficerAccount Officer91010222269101000015Aktif                                 AksiEdit6AO15-91010MKR379081.08.22Ibnu Hajar Saepul MuhtarAccount OfficerAccount Officer91010222269101000016Aktif                                 AksiEdit7AO16-91010Account OfficerAccount Officer91010222269134500025Nonaktif                                 AksiEdit8AO17-91010Account OfficerAccount Officer91010222269134500026Nonaktif                                 AksiEdit9AO18-91010MKR342860.06.22Titi WinarniAccount OfficerAccount Officer91010222269134500027Aktif                                 AksiEdit10AO19-91010MKR379306.08.22Virda Rahman FirmansyahAccount OfficerAccount Officer91010222269101000017Aktif                                 AksiEdit11AO20-91010MKR415707.11.22Gita PurnamasariAccount OfficerAccount Officer91010222269101000018Aktif                                 AksiEdit12AO21-91010MKR438848.02.23Lilim Halimatu ShadiahAccount OfficerAccount Officer91010222269101000019Aktif                                 AksiEdit13AO22-91010MKR438129.02.23Hilda NuryadiAccount OfficerAccount Officer91010222269101000020Aktif                                 AksiEdit14AO23-91010MKR483565.06.23Elsa Nur FadilahAccount OfficerAccount Officer91010222269101000021Aktif                                 AksiEdit15AO4-91010Account OfficerAccount Officer91010222269101000003Nonaktif                                 AksiEdit16AO5-91010MKR207135.11.20PebriyaniAccount OfficerAccount Officer91010222269101000001Aktif                                 AksiEdit17AO6-91010Account OfficerAccount Officer91010222269101000006Nonaktif                                 AksiEdit18AO7-91010MKR269958.08.21Resty Agnia NurohmahAccount OfficerAccount Officer91010222269101000004Aktif                                 AksiEdit19Bukan Role AO / SAOMKR130338.09.19Devi SintiasariFinance Administration OfficerFinance Administration Officer91010Nomor VA Tidak TersediaAktif                                 AksiView20Bukan Role AO / SAOMKR196193.10.20Siti RiyatiFinance Administration OfficerFinance Administration Officer91010Nomor VA Tidak TersediaAktif                                 AksiView21KC-9101022283.01.20Lia AmaliaKepala Cabang MekaarKepala Cabang91010222269101000009Aktif                                 AksiEdit22SAO02-91010MKR202512.11.20Rosah Riska DamayantiSenior Account OfficerSenior Account Officer91010222269101000013Aktif                                 AksiEdit23SAO1-91010Senior Account OfficerSenior Account Officer91010222269101000010Nonaktif                                 AksiEdit24SAO2-91010MKR174652.08.20Fani Nur OktavianiSenior Account OfficerSenior Account Officer91010222269101000008Aktif                                 AksiEdit25SAO3-91010MKR105350.05.19Enjellita AdestaniaSenior Account OfficerSenior Account Officer91010222269101000022Aktif                                 AksiEdit
                                            Showing 1 to 25 of 25 entriesPrevious1Next
                                        
                                    
                                ')]</value>
      <webElementGuid>5ff6ae69-e53e-4dfd-bd9b-975763b42f7b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
