<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LinkManagementPengguna</name>
   <tag></tag>
   <elementGuidId>449cdc25-9cb7-4b3d-b42a-1f7f5065f09c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='manajemen_pengguna']/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#manajemen_pengguna > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>a >> internal:has-text=&quot;Manajemen Pengguna&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>9a56d88c-5b88-40e3-aa0d-16cf35a4490b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                         Manajemen Pengguna
                        
                            
                        
                    </value>
      <webElementGuid>997d1a54-d02e-46ca-8d84-727e8b6b662c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;manajemen_pengguna&quot;)/a[1]</value>
      <webElementGuid>704ad9b3-bef7-485f-aceb-973947a5dd04</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='manajemen_pengguna']/a</value>
      <webElementGuid>b05d0831-7299-4241-b601-6ceabab57d3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pengelolaan AO'])[1]/following::a[1]</value>
      <webElementGuid>1fe2f90d-4b20-4de2-873e-85a97ac04a0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penugasan AO'])[1]/following::a[2]</value>
      <webElementGuid>9f2607e1-bad2-4e41-acd6-5cc538111712</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar Pengguna'])[1]/preceding::a[1]</value>
      <webElementGuid>a58a7354-a8e5-4bbd-8e0b-7b4597f47833</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[13]/a</value>
      <webElementGuid>e0faddc6-e6f6-48f8-8370-8f86dbb53f40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '
                         Manajemen Pengguna
                        
                            
                        
                    ' or . = '
                         Manajemen Pengguna
                        
                            
                        
                    ')]</value>
      <webElementGuid>27eec369-4f6e-437f-91eb-54bd675aee47</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
