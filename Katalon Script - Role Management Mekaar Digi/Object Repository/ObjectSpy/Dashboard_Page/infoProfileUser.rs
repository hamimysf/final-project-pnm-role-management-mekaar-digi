<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>infoProfileUser</name>
   <tag></tag>
   <elementGuidId>17e1301c-7623-438a-9f13-099d1d8312bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[1]/following::a[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.btn.dropdown-toggle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=link[name=&quot; Finance Administration Officer - Area Majalengka 4 | Unit Cikijing &quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>dc5e8415-3ab1-4be9-9964-42de0e3415fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn dropdown-toggle</value>
      <webElementGuid>07b14cb9-eb97-4b30-8715-7c1ec6d1b7db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>1e07ea53-62cc-4c14-b17c-ea4756ae96a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>cca5efed-9145-4b00-9b8c-9f219b03f4df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Finance Administration Officer | Area Majalengka 4 | Unit Cikijing</value>
      <webElementGuid>d0c81881-66b4-427a-a331-a503c43f4075</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                   
                  
                      Finance Administration Officer - Area Majalengka 4 | Unit Cikijing 
                  
              </value>
      <webElementGuid>a531311c-6040-4f0b-91ac-177a8c8c7f52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;skin-blue sidebar-mini&quot;]/div[@class=&quot;wrapper&quot;]/header[@class=&quot;main-header&quot;]/nav[@class=&quot;navbar navbar-static-top&quot;]/div[@class=&quot;navbar-custom-menu&quot;]/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;dropdown nav-item dropdown&quot;]/a[@class=&quot;btn dropdown-toggle&quot;]</value>
      <webElementGuid>5574cdd0-9703-4db1-a141-8ce533157dc3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[1]/following::a[2]</value>
      <webElementGuid>aef86c75-fee4-4323-977c-22b22f958e9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MEKAAR'])[1]/following::a[3]</value>
      <webElementGuid>744794ac-fbd4-45bc-a238-528a4191dc95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='User Profile'])[1]/preceding::a[1]</value>
      <webElementGuid>c1ded823-3be3-4e80-b43c-150ddca18868</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Password'])[1]/preceding::a[2]</value>
      <webElementGuid>db33a7ac-c439-4260-81c2-e524d49d7def</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Finance Administration Officer - Area Majalengka 4 | Unit Cikijing']/parent::*</value>
      <webElementGuid>9b9697f0-b3b6-456b-955d-c6110586646b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[3]</value>
      <webElementGuid>a9ca09a0-8f1e-43e7-a0fb-e10de71d8904</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a</value>
      <webElementGuid>c0f8deab-1463-40ab-987c-494de865cf14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and @title = 'Finance Administration Officer | Area Majalengka 4 | Unit Cikijing' and (text() = '
                   
                  
                      Finance Administration Officer - Area Majalengka 4 | Unit Cikijing 
                  
              ' or . = '
                   
                  
                      Finance Administration Officer - Area Majalengka 4 | Unit Cikijing 
                  
              ')]</value>
      <webElementGuid>131c9b79-1229-46e4-aa26-c39919a7772e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
