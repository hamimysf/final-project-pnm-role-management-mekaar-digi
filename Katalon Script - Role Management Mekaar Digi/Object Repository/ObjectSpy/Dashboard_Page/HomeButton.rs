<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>HomeButton</name>
   <tag></tag>
   <elementGuidId>a5ef18b1-e21f-4bd9-adc8-cb0e2fb06fb9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='MKR'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.logo-lg</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=link[name=&quot;MEKAAR Digi&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b6560dbd-574f-4393-b1c2-d83426d18ab9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>logo-lg</value>
      <webElementGuid>97af24e5-f02e-4232-9b1e-0ba17163ef2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>MEKAAR Digi</value>
      <webElementGuid>c89f5e30-16ea-4029-aebf-b045f3b1ce0e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;skin-blue sidebar-mini&quot;]/div[@class=&quot;wrapper&quot;]/header[@class=&quot;main-header&quot;]/a[@class=&quot;logo&quot;]/span[@class=&quot;logo-lg&quot;]</value>
      <webElementGuid>d4d18764-ad73-45fa-a826-9fa0cdca9b69</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MKR'])[1]/following::span[1]</value>
      <webElementGuid>91981b15-72dc-4d25-9518-65782b7aab4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[1]/preceding::span[1]</value>
      <webElementGuid>3063a0d3-4a36-45ff-87b3-15497be03b4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Digi']/parent::*</value>
      <webElementGuid>6639fdf3-1b5e-498d-8f1f-379653bdc837</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]</value>
      <webElementGuid>1b316a3f-cb9c-487f-8dd3-8e9ffa514d63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'MEKAAR Digi' or . = 'MEKAAR Digi')]</value>
      <webElementGuid>a0a372d8-e499-465a-8873-2699e9c9fb04</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
