<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>UserInfoDashboard</name>
   <tag></tag>
   <elementGuidId>a7ed92a0-d727-47d9-80e0-1082f0f9f71f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Report Center'])[1]/following::small[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>small</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;Finance Administration Officer Area Majalengka 4 | Unit Cikijing&quot;s</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>small</value>
      <webElementGuid>37b65b92-6bac-4c92-a320-f9e5f101539c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Finance Administration Officer Area Majalengka 4 | Unit Cikijing </value>
      <webElementGuid>65c3c184-f49c-44d9-8793-22bc0615dd6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;skin-blue sidebar-mini&quot;]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-wrapper&quot;]/section[@class=&quot;content-header&quot;]/h1[1]/small[1]</value>
      <webElementGuid>6f977b75-f83a-4c6f-ae66-d5cb319bb877</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Report Center'])[1]/following::small[1]</value>
      <webElementGuid>b371e825-65f9-4e68-a08c-10dd3658b972</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Master Kondisi Rumah'])[1]/following::small[1]</value>
      <webElementGuid>4d2acd73-8510-4544-8d2a-ec103bdb3d08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PKM Back Office'])[1]/preceding::small[1]</value>
      <webElementGuid>7ecac5f6-300a-4c34-8d39-0937e5b1d1e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Finance Administration Officer Area Majalengka 4 | Unit Cikijing']/parent::*</value>
      <webElementGuid>6a37baaf-68c7-4e00-afaa-610995303939</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//small</value>
      <webElementGuid>07eeb2d0-b39f-4d59-a013-f78190b5ae0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//small[(text() = ' Finance Administration Officer Area Majalengka 4 | Unit Cikijing ' or . = ' Finance Administration Officer Area Majalengka 4 | Unit Cikijing ')]</value>
      <webElementGuid>51522c46-afb4-4594-a848-7588ec1d4c34</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
