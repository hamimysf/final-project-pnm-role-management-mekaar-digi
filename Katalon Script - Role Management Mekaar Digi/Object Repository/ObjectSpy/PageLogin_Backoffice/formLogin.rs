<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>formLogin</name>
   <tag></tag>
   <elementGuidId>842aa3df-85b8-4a78-a02e-16e48a772fac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Mekaar'])[1]/following::div[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.login-box-body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;Sign in terlebih dahulu untuk memulai session Show Password Login Ubah Password &quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6042dc28-8e3c-4f1e-aa32-a6b1047c665f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login-box-body</value>
      <webElementGuid>364839f6-31c4-43fa-9651-7a4974f1da1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Sign in terlebih dahulu untuk memulai session
                        
                            
                                
                                    
                                
                            
                            

                                                                                                
                                
                                    
                                        
                                        
                                    
                                    
                                        
                                        
                                         Show Password
                                        

                                    

                                
                                
                                    

                                    
                                        
                                        
                                    
                                
                                  Ubah Password
                            
                        
                        © 2020 Permodalan Nasional MadaniDeveloped by Teknologi Informasi (TIF)
                    </value>
      <webElementGuid>d2ea8876-0991-46b9-bda9-5a6bea059e1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;login-page&quot;]/div[@class=&quot;fullscreen_bg&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;login-box&quot;]/div[@class=&quot;login-box-body&quot;]</value>
      <webElementGuid>6935a614-cdf5-4b84-b834-91a718044a88</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mekaar'])[1]/following::div[1]</value>
      <webElementGuid>18d7675d-4ae6-4a33-8cf8-fbd990543bcd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]</value>
      <webElementGuid>571ce231-17c4-4c38-abbe-89763110d487</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                        Sign in terlebih dahulu untuk memulai session
                        
                            
                                
                                    
                                
                            
                            

                                                                                                
                                
                                    
                                        
                                        
                                    
                                    
                                        
                                        
                                         Show Password
                                        

                                    

                                
                                
                                    

                                    
                                        
                                        
                                    
                                
                                  Ubah Password
                            
                        
                        © 2020 Permodalan Nasional MadaniDeveloped by Teknologi Informasi (TIF)
                    ' or . = '
                        
                        Sign in terlebih dahulu untuk memulai session
                        
                            
                                
                                    
                                
                            
                            

                                                                                                
                                
                                    
                                        
                                        
                                    
                                    
                                        
                                        
                                         Show Password
                                        

                                    

                                
                                
                                    

                                    
                                        
                                        
                                    
                                
                                  Ubah Password
                            
                        
                        © 2020 Permodalan Nasional MadaniDeveloped by Teknologi Informasi (TIF)
                    ')]</value>
      <webElementGuid>cdc10dde-77c8-425a-897f-0f1f8b3f0bc9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
