<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>pesanLogin</name>
   <tag></tag>
   <elementGuidId>4a92481a-f78d-4aeb-a288-c5bcf89cae09</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Mekaar'])[1]/following::p[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.alert.alert-danger</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;Mohon maaf, user tidak terdaftar!&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>a2037448-eee8-4007-bbda-ca7bd46c552b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger</value>
      <webElementGuid>ba293829-438a-4c09-a220-e26eec649211</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Mohon maaf, user tidak terdaftar!</value>
      <webElementGuid>4fbe54e8-92d7-49bf-8d5e-44bf96cd54a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;login-page&quot;]/div[@class=&quot;fullscreen_bg&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;login-box&quot;]/div[@class=&quot;login-box-body&quot;]/div[@class=&quot;login-panel panel panel-default&quot;]/div[@class=&quot;panel-body&quot;]/p[@class=&quot;alert alert-danger&quot;]</value>
      <webElementGuid>84824087-9129-4f6e-8225-f645fe79e0df</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mekaar'])[1]/following::p[3]</value>
      <webElementGuid>0cf3a560-6625-48f6-a5fa-dc9137c8607d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show Password'])[1]/preceding::p[1]</value>
      <webElementGuid>6515aecf-6266-4232-bc71-eaf617470965</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ubah Password'])[1]/preceding::p[1]</value>
      <webElementGuid>13c96541-8302-4f7e-8359-4f1130d52dec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Mohon maaf, user tidak terdaftar!']/parent::*</value>
      <webElementGuid>e7968dfa-a036-4379-86a7-ac85d51d4568</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/p</value>
      <webElementGuid>b4b27a2f-1dca-4cff-b59f-f5e1331bea21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Mohon maaf, user tidak terdaftar!' or . = 'Mohon maaf, user tidak terdaftar!')]</value>
      <webElementGuid>ac0810c0-4fe2-4fdf-b81e-abab2e6958db</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
