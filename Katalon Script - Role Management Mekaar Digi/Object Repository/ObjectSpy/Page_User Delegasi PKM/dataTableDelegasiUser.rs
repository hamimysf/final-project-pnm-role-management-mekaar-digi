<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dataTableDelegasiUser</name>
   <tag></tag>
   <elementGuidId>7a27470e-6361-4142-8eea-c0304f2ad385</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='datatable-user-mobile-delegasi_wrapper']/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.row > div.col-sm-12</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#datatable-user-mobile-delegasi_wrapper div >> internal:has-text=&quot;NoCabang RequestRole yang DibutuhkanJumlahTanggal MulaiTanggal BerakhirStatusTan&quot;i >> nth=1</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>24c74c19-ef92-4b33-9ab0-a3782d56682a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-sm-12</value>
      <webElementGuid>5bf6d0a8-08a9-423a-be58-3549a87ba688</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                    
                                                        NoCabang RequestRole yang DibutuhkanJumlahTanggal MulaiTanggal BerakhirStatusTanggal RequestTanggal Terpenuhi
                                                    
                                                    
                                                No data available in table</value>
      <webElementGuid>2870d529-abfd-4e74-9b5c-bb51f25a1ad8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;datatable-user-mobile-delegasi_wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-12&quot;]</value>
      <webElementGuid>a38664a4-ee24-4863-b038-cd8ee078f147</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='datatable-user-mobile-delegasi_wrapper']/div[2]/div</value>
      <webElementGuid>238c836a-9d26-4f1d-bd01-b43b6f35321c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search:'])[1]/following::div[2]</value>
      <webElementGuid>5e1d781b-291c-4432-aa81-c7458643c0c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/div[2]/div</value>
      <webElementGuid>86eecd0c-326a-42fd-a48b-b367c834f35b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                                    
                                                        NoCabang RequestRole yang DibutuhkanJumlahTanggal MulaiTanggal BerakhirStatusTanggal RequestTanggal Terpenuhi
                                                    
                                                    
                                                No data available in table' or . = '
                                                    
                                                        NoCabang RequestRole yang DibutuhkanJumlahTanggal MulaiTanggal BerakhirStatusTanggal RequestTanggal Terpenuhi
                                                    
                                                    
                                                No data available in table')]</value>
      <webElementGuid>c6cf7a9d-6108-4a7c-ad0f-ccfef6ae027c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
