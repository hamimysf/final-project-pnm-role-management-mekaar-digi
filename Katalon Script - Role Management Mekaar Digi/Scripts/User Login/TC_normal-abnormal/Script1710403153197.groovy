import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

if (message == '') {
    WebUI.navigateToUrl('http://devapipkm.pnm.co.id/pkmBackofficeDev/')

    WebUI.verifyElementVisible(findTestObject('ObjectSpy/PageLogin_Backoffice/formLogin'))

    WebUI.setText(findTestObject('ObjectSpy/PageLogin_Backoffice/inputUsername'), username)

    WebUI.setEncryptedText(findTestObject('ObjectSpy/PageLogin_Backoffice/inputPasssword'), password)

    WebUI.delay(1, FailureHandling.STOP_ON_FAILURE)

    WebUI.click(findTestObject('ObjectSpy/PageLogin_Backoffice/submitLogin'))

    WebUI.verifyElementVisible(findTestObject('ObjectSpy/Dashboard_Page/HomeButton'))

    text = WebUI.getText(findTestObject('ObjectSpy/Dashboard_Page/UserInfoDashboard'))

    text2 = WebUI.getText(findTestObject('ObjectSpy/Dashboard_Page/infoProfileUser'))

    WebUI.verifyMatch(text, ('^' + role) + '.*', true, FailureHandling.STOP_ON_FAILURE)

    WebUI.verifyMatch(text2, ('^' + role) + '.*', true, FailureHandling.STOP_ON_FAILURE)
} else {
    WebUI.navigateToUrl('http://devapipkm.pnm.co.id/pkmBackofficeDev/')

    WebUI.verifyElementVisible(findTestObject('ObjectSpy/PageLogin_Backoffice/formLogin'))

    WebUI.setText(findTestObject('ObjectSpy/PageLogin_Backoffice/inputUsername'), username)

    if (username == '123') {
        WebUI.navigateToUrl('http://devapipkm.pnm.co.id/pkmBackofficeDev/home', FailureHandling.STOP_ON_FAILURE)

        WebUI.waitForPageLoad(1, FailureHandling.STOP_ON_FAILURE)

        WebUI.verifyElementText(findTestObject('ObjectSpy/PageLogin_Backoffice/pesanLogin'), message)
    } else if (username == '321') {
        WebUI.navigateToUrl('http://devapipkm.pnm.co.id/pkmBackofficeDev/pkm/usercontrol', FailureHandling.STOP_ON_FAILURE)

        WebUI.waitForPageLoad(1, FailureHandling.STOP_ON_FAILURE)

        WebUI.verifyElementText(findTestObject('ObjectSpy/PageLogin_Backoffice/pesanLogin'), message)
    } else {
        WebUI.setEncryptedText(findTestObject('ObjectSpy/PageLogin_Backoffice/inputPasssword'), password)

        WebUI.delay(1, FailureHandling.STOP_ON_FAILURE)

        WebUI.click(findTestObject('ObjectSpy/PageLogin_Backoffice/submitLogin'))

        WebUI.verifyElementText(findTestObject('ObjectSpy/PageLogin_Backoffice/pesanLogin'), message)
    }
}

WebUI.closeBrowser()

