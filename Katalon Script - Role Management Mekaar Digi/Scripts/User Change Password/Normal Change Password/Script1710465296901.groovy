import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
// Lib untuk manipulasi file excel
import org.apache.poi.ss.usermodel.*
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.apache.poi.ss.usermodel.BorderStyle
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.HorizontalAlignment

// Path ke file Excel
String userDir = System.getProperty('user.dir')
String filePath = userDir + '/Data Files/DataTestFile.xlsx'
// Load file Excel
FileInputStream file = new FileInputStream(filePath)
XSSFWorkbook workbook = new XSSFWorkbook(file)
// Mendapatkan sheet yang sesuai
Sheet sheet = workbook.getSheet("Data Change Password")
// Mendapatkan baris dan sel yang sesuai
int indxRow = Integer.parseInt(index)
Row row = sheet.getRow(indxRow) // Baris ke-n
Cell cell = row.getCell(7) // Kolom ke-10


// Mengubah nilai di sel yang dipilih
if(status == "change") {
	cell.setCellValue("back")
}
else {
	cell.setCellValue("change")
}

// Menyimpan perubahan ke dalam file
FileOutputStream outFile = new FileOutputStream(new File(filePath));
workbook.write(outFile);
outFile.close();

// Menutup workbook
workbook.close();

